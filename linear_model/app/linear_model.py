import os
import sys
import types
import pickle
import marshal
import warnings
import numpy as np
import pandas as pd
import seaborn as sns
from scipy import stats
import matplotlib.pyplot as plt
from xgboost import XGBRegressor
from sklearn.impute import SimpleImputer
from sklearn.preprocessing import LabelEncoder
from sklearn.tree import DecisionTreeRegressor
from sklearn.metrics import mean_squared_error
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import LinearRegression
from sklearn.neighbors import KNeighborsRegressor
from sklearn.ensemble import RandomForestRegressor
from sklearn.model_selection import train_test_split
from xpresso.ai.core.utils.jupyter_experiment_utils import xpresso_save_plot
from xpresso.ai.core.commons.exceptions.xpr_exceptions import MountPathNoneType


if not os.environ["XPRESSO_MOUNT_PATH"]:
    raise MountPathNoneType
PICKLE_PATH = os.path.join(os.environ["XPRESSO_MOUNT_PATH"], "xjp_store")

if not os.path.exists(PICKLE_PATH):
    os.makedirs(PICKLE_PATH, mode=0o777, exist_ok=True)

try:
    train_data = pickle.load(open(f"{PICKLE_PATH}/train_data.pkl", "rb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    test_data = pickle.load(open(f"{PICKLE_PATH}/test_data.pkl", "rb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    Xtrain_Zs = pickle.load(open(f"{PICKLE_PATH}/Xtrain_Zs.pkl", "rb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    ytrain_Zs = pickle.load(open(f"{PICKLE_PATH}/ytrain_Zs.pkl", "rb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    ytrain_log = pickle.load(open(f"{PICKLE_PATH}/ytrain_log.pkl", "rb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    yvalid_log = pickle.load(open(f"{PICKLE_PATH}/yvalid_log.pkl", "rb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    Xvalid_scaled = pickle.load(open(f"{PICKLE_PATH}/Xvalid_scaled.pkl", "rb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    Xtrain_scaled = pickle.load(open(f"{PICKLE_PATH}/Xtrain_scaled.pkl", "rb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))

## $xpr_param_component_name = linear_model	
## $xpr_param_component_type = pipeline_job
## $xpr_param_global_variables = ["train_data","test_data","Xtrain_Zs","ytrain_Zs","ytrain_log","yvalid_log","Xvalid_scaled","Xtrain_scaled"]
# Models from Scikit-Learn



# Put models in a dictionary
test_models = {
    "Linear Regression": LinearRegression(),
    "Decision Tree    ": DecisionTreeRegressor(random_state=0),
    "Random Forest    ": RandomForestRegressor(random_state=0),
    "KNN              ": KNeighborsRegressor(),
    "XGBoost          ": XGBRegressor(random_state=0)
}

# Create a function to fit and score models
def score_dataset(models, X_t, X_v, y_t, y_v, lg=False):
    
    # Make a dictionary to keep model scores and RMSE 
    model_scores = {}
    rmse_scores  = {}
    
    print("Train")
    
    # Loop through models
    for modelname, model in models.items():
        
        # Fit the model to the data
        model.fit(X_t, y_t)
        preds_train = model.predict(X_t)
        preds = model.predict(X_v)
        
        print(modelname + ":" + str(model.score(X_t, y_t)))
        
        # Evaluate the model and append its score to model_scores
        model_scores[modelname] = model.score(X_v, y_v)
        
        if lg:
            preds = np.exp(preds)
            preds_train = np.exp(preds_train)
            yv = np.exp(y_v)
            yt = np.exp(y_t)
        else:
            yt = y_t.copy()
            yv = y_v.copy()
        
        print(modelname + ":" + str(np.sqrt(mean_squared_error(yt, preds_train))))
        rmse_scores[modelname] = np.sqrt(mean_squared_error(yv, preds))
        
    return model_scores, rmse_scores

modelScore, rmseScore = score_dataset(test_models, Xtrain_Zs, Xvalid_scaled, ytrain_log, yvalid_log,True)
modelScore , rmseScore
XGBmodel = XGBRegressor(n_estimators=2000, learning_rate=0.1, n_jobs=4, random_state = 101)
XGBmodel.fit(Xtrain_Zs, ytrain_Zs, 
            # early_stopping_rounds = 5, 355.71612921117776 2000, 354.7684598810383
             eval_set=[(Xvalid_scaled, yvalid)],
             eval_metric='rmse',
             verbose=False)
print("XGBoost Regression R^2 Score: ", XGBmodel.score(Xtrain_Zs, ytrain_Zs))
print("XGBoost Regression Test R^2 Score: ", XGBmodel.score(Xvalid_scaled, yvalid))

xgbReg_ytrain = XGBmodel.predict(Xtrain_Zs)
print("XGBoost Regression RMSE train: ", np.sqrt(mean_squared_error(xgbReg_ytrain, ytrain_Zs)))

xgbReg_ycvpred = XGBmodel.predict(Xvalid_scaled)
print("XGBoost Regression RMSE test: ", np.sqrt(mean_squared_error(xgbReg_ycvpred, yvalid)))

try:
    pickle.dump(train_data, open(f"{PICKLE_PATH}/train_data.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    pickle.dump(test_data, open(f"{PICKLE_PATH}/test_data.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    pickle.dump(Xtrain_Zs, open(f"{PICKLE_PATH}/Xtrain_Zs.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    pickle.dump(ytrain_Zs, open(f"{PICKLE_PATH}/ytrain_Zs.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    pickle.dump(ytrain_log, open(f"{PICKLE_PATH}/ytrain_log.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    pickle.dump(yvalid_log, open(f"{PICKLE_PATH}/yvalid_log.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    pickle.dump(Xvalid_scaled, open(f"{PICKLE_PATH}/Xvalid_scaled.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    pickle.dump(Xtrain_scaled, open(f"{PICKLE_PATH}/Xtrain_scaled.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))

