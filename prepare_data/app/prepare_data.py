import os
import sys
import types
import pickle
import marshal
import warnings
import numpy as np
import pandas as pd
import seaborn as sns
from scipy import stats
import matplotlib.pyplot as plt
from xgboost import XGBRegressor
from sklearn.impute import SimpleImputer
from sklearn.preprocessing import LabelEncoder
from sklearn.tree import DecisionTreeRegressor
from sklearn.metrics import mean_squared_error
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import LinearRegression
from sklearn.neighbors import KNeighborsRegressor
from sklearn.ensemble import RandomForestRegressor
from sklearn.model_selection import train_test_split
from xpresso.ai.core.utils.jupyter_experiment_utils import xpresso_save_plot
from xpresso.ai.core.commons.exceptions.xpr_exceptions import MountPathNoneType


if not os.environ["XPRESSO_MOUNT_PATH"]:
    raise MountPathNoneType
PICKLE_PATH = os.path.join(os.environ["XPRESSO_MOUNT_PATH"], "xjp_store")

if not os.path.exists(PICKLE_PATH):
    os.makedirs(PICKLE_PATH, mode=0o777, exist_ok=True)

try:
    train_data = pickle.load(open(f"{PICKLE_PATH}/train_data.pkl", "rb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    test_data = pickle.load(open(f"{PICKLE_PATH}/test_data.pkl", "rb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    Xtrain_Zs = pickle.load(open(f"{PICKLE_PATH}/Xtrain_Zs.pkl", "rb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    ytrain_log = pickle.load(open(f"{PICKLE_PATH}/ytrain_log.pkl", "rb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    yvalid_log = pickle.load(open(f"{PICKLE_PATH}/yvalid_log.pkl", "rb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    Xvalid_scaled = pickle.load(open(f"{PICKLE_PATH}/Xvalid_scaled.pkl", "rb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    yfull = pickle.load(open(f"{PICKLE_PATH}/yfull.pkl", "rb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    Xfull = pickle.load(open(f"{PICKLE_PATH}/Xfull.pkl", "rb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    num_columns = pickle.load(open(f"{PICKLE_PATH}/num_columns.pkl", "rb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    cat_columns = pickle.load(open(f"{PICKLE_PATH}/cat_columns.pkl", "rb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    X1 = pickle.load(open(f"{PICKLE_PATH}/X1.pkl", "rb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    Xtrain = pickle.load(open(f"{PICKLE_PATH}/Xtrain.pkl", "rb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    Xvalid = pickle.load(open(f"{PICKLE_PATH}/Xvalid.pkl", "rb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    ytrain = pickle.load(open(f"{PICKLE_PATH}/ytrain.pkl", "rb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    yvalid = pickle.load(open(f"{PICKLE_PATH}/yvalid.pkl", "rb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    X_num = pickle.load(open(f"{PICKLE_PATH}/X_num.pkl", "rb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    na_num = pickle.load(open(f"{PICKLE_PATH}/na_num.pkl", "rb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    X_ntrain = pickle.load(open(f"{PICKLE_PATH}/X_ntrain.pkl", "rb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    X_nvalid = pickle.load(open(f"{PICKLE_PATH}/X_nvalid.pkl", "rb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    imputed_X_ntrain = pickle.load(open(f"{PICKLE_PATH}/imputed_X_ntrain.pkl", "rb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    imputed_X_nvalid = pickle.load(open(f"{PICKLE_PATH}/imputed_X_nvalid.pkl", "rb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    Xtrain_u1 = pickle.load(open(f"{PICKLE_PATH}/Xtrain_u1.pkl", "rb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    Xvalid_u1 = pickle.load(open(f"{PICKLE_PATH}/Xvalid_u1.pkl", "rb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    X_cat = pickle.load(open(f"{PICKLE_PATH}/X_cat.pkl", "rb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    na_cat = pickle.load(open(f"{PICKLE_PATH}/na_cat.pkl", "rb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    imputed_X_ctrain = pickle.load(open(f"{PICKLE_PATH}/imputed_X_ctrain.pkl", "rb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    imputed_X_cvalid = pickle.load(open(f"{PICKLE_PATH}/imputed_X_cvalid.pkl", "rb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    label_Xtrain = pickle.load(open(f"{PICKLE_PATH}/label_Xtrain.pkl", "rb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    label_Xvalid = pickle.load(open(f"{PICKLE_PATH}/label_Xvalid.pkl", "rb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    corr_col_todrop = pickle.load(open(f"{PICKLE_PATH}/corr_col_todrop.pkl", "rb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    corr_Xtrain = pickle.load(open(f"{PICKLE_PATH}/corr_Xtrain.pkl", "rb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    corr_Xvalid = pickle.load(open(f"{PICKLE_PATH}/corr_Xvalid.pkl", "rb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    straindata = pickle.load(open(f"{PICKLE_PATH}/straindata.pkl", "rb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    svaliddata = pickle.load(open(f"{PICKLE_PATH}/svaliddata.pkl", "rb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    Xtrain_scaled = pickle.load(open(f"{PICKLE_PATH}/Xtrain_scaled.pkl", "rb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    Xvalid_scaled = pickle.load(open(f"{PICKLE_PATH}/Xvalid_scaled.pkl", "rb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    modelScore = pickle.load(open(f"{PICKLE_PATH}/modelScore.pkl", "rb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    rmseScore = pickle.load(open(f"{PICKLE_PATH}/rmseScore.pkl", "rb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    ytrain_Zs = pickle.load(open(f"{PICKLE_PATH}/ytrain_Zs.pkl", "rb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    ytrain_log = pickle.load(open(f"{PICKLE_PATH}/ytrain_log.pkl", "rb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    yvalid_log = pickle.load(open(f"{PICKLE_PATH}/yvalid_log.pkl", "rb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))

## $xpr_param_component_name = prepare_data
## $xpr_param_component_type = pipeline_job 
## $xpr_param_global_variables = ["train_data","test_data","Xtrain_Zs","ytrain_log","yvalid_log","Xvalid_scaled","yfull","Xfull","num_columns","cat_columns","X1","Xtrain","Xvalid","ytrain", "yvalid","X_num","na_num","X_ntrain","X_nvalid","imputed_X_ntrain","imputed_X_nvalid","Xtrain_u1","Xvalid_u1","X_cat","na_cat","imputed_X_ctrain","imputed_X_cvalid" ,"label_Xtrain", "label_Xvalid","corr_col_todrop","corr_Xtrain","corr_Xvalid","straindata","svaliddata","Xtrain_scaled","Xvalid_scaled","modelScore","rmseScore","ytrain_Zs","ytrain_log","yvalid_log"]

# Get the count of categorical and numerical features in the training dataset.
cat = len(train_data.select_dtypes(include=['object']).columns)
num = len(train_data.select_dtypes(exclude=['object']).columns)
#cat,num
print('Total Features: ', cat, 'categorical', '+', num, 'numerical', '=', cat+num, 'features')
# Data Pre-processing
# 1. Remove duplicates
# 2. Split training dataset to Xfull and yfull
# 3. Find numerical columns
# 4. Find categorical columns and remove high cardinality(unique values more than 15)
#    Also, remove categorical columns with single unique values
# 5. Updating X1 after clearing out high cardinal and single unique categorical variables from Xfull.
#    (Same step has to be done to test data)
# 6. Check for missing values in X1 training dataset.
# 7. Split X1,yfull to training and validation datas
# 8. Find best approach to handle missing data with numerical columns
# 9. Find best approach to handle missing data with categorical columns(if imputed, then we have to encode it), then check RMSE
# 10.Check or strong correlations between parameters. If so, then remove one of the parameters.
# Function for removing duplicates from housing rental full training dataset
def remove_duplicates():
    train_data.drop_duplicates(inplace=True)
    
    # Find the number of rows and columns inside the training dataset
    print("Shape after dropping duplicates as Row index:" ,train_data.shape)
# Step 1 :- Removing duplicate rows/examples from training dataset to avoid bias.
remove_duplicates()
# Step 2 :- Split training data to X(feature variables) and y(target variable)
yfull = train_data.rent
Xfull = train_data.drop(['rent'],axis=1)
# Step 3 :- Get numerical columns
num_columns = [col for col in Xfull.columns if Xfull[col].dtype not in ['object']]
print(len(num_columns),num_columns)

# Total of 28 numerical variables are present.
# Step 4 :- Find categorical columns and remove high cardinality(unique values more than 15)
#           Also, remove categorical columns with single unique values

# Print all categorical columns first
print(Xfull.select_dtypes(include='object').columns)

# There are 5 categorical variables in the full training dataset.

# Get categorical columns from Xfull, which have low cardinality and have more than one unique values.
cat_columns = [col for col in Xfull.columns 
               if Xfull[col].dtype in ['object'] and 
               Xfull[col].nunique() <15 and 
               Xfull[col].nunique() > 1]
print(len(cat_columns),cat_columns)

# After removing high cardinal columns(county, city,address) and columns with only 1 unique value(for state),
# we are only left with just one categorical column.
# Also, it will be ok to lose address, county, city and state as we have numerical variables latitude and longitude.
# Step 5 :- Updating X1 after clearing out high cardinal and single unique categorical variables from Xfull.
X1 = Xfull[cat_columns+num_columns].copy()
print("Shape after initial preprocessing:" ,X1.shape)
#X1.head()

# Divide entire training dataset(X1,yfull) into training and validation subsets
Xtrain, Xvalid, ytrain, yvalid = train_test_split(X1, yfull, train_size=0.8, test_size=0.2, random_state=0)
# Create X_num without any objects to get numerical missing values variables
X_num = X1.select_dtypes(exclude=['object'])

na_num = [var for var in X_num.columns if X_num[var].isnull().sum() > 0]
print("Missing values(Numerical) column:", na_num)

## Divide numerical training dataset into training and validation subsets
#X_ntrain, X_nvalid, y_ntrain, y_nvalid = train_test_split(X_num, yfull, train_size=0.8, test_size=0.2, random_state=0)

X_ntrain = Xtrain.select_dtypes(exclude=['object'])
X_nvalid = Xvalid.select_dtypes(exclude=['object'])

#X_ntrain.head()

def imputation(X_t, X_v, y_t, y_v, na_var, s='mean', f=None, buildOn=True):
    
    # Going to impute only, those missing value columns. Else, the entire datatype will get wrong.
    naX_t = X_t[na_var].copy()
    naX_v = X_v[na_var].copy()
    
    # Imputation
    imputer = SimpleImputer(strategy=s, fill_value=f)
    imputed_X_t = pd.DataFrame(imputer.fit_transform(naX_t))
    imputed_X_v = pd.DataFrame(imputer.transform(naX_v))
    
    # Imputation removed column names; put them back
    imputed_X_t.columns = naX_t.columns
    imputed_X_v.columns = naX_v.columns
    
    # Imputation removed index names; put them back
    imputed_X_t.set_index(naX_t.index,inplace=True)
    imputed_X_v.set_index(naX_v.index,inplace=True)
    
    # Updating the given training and validation dataset after imputation
    imputed_X_train = pd.concat([imputed_X_t, X_t.drop(na_var,axis=1)],axis=1)
    imputed_X_valid = pd.concat([imputed_X_v, X_v.drop(na_var,axis=1)],axis=1)
    
#    if buildOn:
#        rmse = rf_score_dataset(imputed_X_train,imputed_X_valid, y_t, y_v)
#        print("RMSE from Approach 2 (Imputation-"+ s + "):", rmse)
#        return rmse,imputed_X_train,imputed_X_valid
#    else:
#        return imputed_X_train,imputed_X_valid

    return imputed_X_train,imputed_X_valid
# Find the best approach in imputation by comparing different imputation strategies like
# median, most_frequent, constant
#nimpute_RMSE, imputed_X_ntrain,imputed_X_nvalid = imputation(X_ntrain,X_nvalid,ytrain,yvalid,na_num,'most_frequent')
imputed_X_ntrain,imputed_X_nvalid = imputation(X_ntrain,X_nvalid,ytrain,yvalid,na_num,'most_frequent')
## Now, update Xtrain, Xvalid zipcode column with values from imputed_X_ntrain and imputed_X_nvalid
#Xtrain_u1 = pd.concat([Xtrain[cat_columns],imputed_X_ntrain],axis=1)
#Xvalid_u1 = pd.concat([Xvalid[cat_columns],imputed_X_nvalid],axis=1)

# Now, update Xtrain, Xvalid zipcode column with values from imputed_X_ntrain_plus and imputed_X_nvalid_plus
Xtrain_u1 = pd.concat([Xtrain[cat_columns],imputed_X_ntrain],axis=1)
Xvalid_u1 = pd.concat([Xvalid[cat_columns],imputed_X_nvalid],axis=1)

#Xtrain_u1.head()
#Xvalid_u1.head()
#missingValuesInfo(Xtrain_u1)
# Create X_cat without any numerical variables to get the number of categorical missing values
X_cat = X1.select_dtypes(include=['object'])

na_cat = [var for var in X_cat.columns if X_cat[var].isnull().sum() > 0]
print("Missing values(Categorical) column:", na_cat)
# Approaches
#------------
# 1.(a). Drop columns with missing values (simplest approach)
#   (b). Drop Categorical Variables
# 2. Imputation (a. mostfrequent, b. constant)
# 3. Extended Imputation
# 4. K-NN

def labelEncoding(X_t, X_v, y_t, y_v,cat_cols):
    # Make copy to avoid changing original data
    label_X_train = X_t.copy()
    label_X_valid = X_v.copy()
    
    # Apply label encoder to each column with categorical data
    label_encoder = LabelEncoder()
    
    for col in cat_cols:
        label_X_train[col] = label_encoder.fit_transform(X_t[col])
        label_X_valid[col] = label_encoder.transform(X_v[col])
        
#    rmse = rf_score_dataset(label_X_train, label_X_valid, y_t, y_v)
#    print("RMSE from Approach 2 (Label Encoding):", rmse)
#    
#    return rmse, label_X_train, label_X_valid

    return label_X_train, label_X_valid
imputed_X_ctrain,imputed_X_cvalid     = imputation(Xtrain_u1, Xvalid_u1, ytrain, yvalid, cat_columns,'most_frequent',None,False)
#labelRMSE, label_Xtrain, label_Xvalid = labelEncoding(imputed_X_ctrain, imputed_X_cvalid, ytrain, yvalid,cat_columns)
label_Xtrain, label_Xvalid = labelEncoding(imputed_X_ctrain, imputed_X_cvalid, ytrain, yvalid,cat_columns)

#RMSE from Approach 2 (Label Encoding): 389.76461144850356
#RMSE from Approach 3 (One-Hot Encoding): 389.8877617399726
# Drop columns with correlation more than 50%. 
# But 'bath', 'bed', 'sqft', 'Crime_Rate', 'Census_MedianIncome'
#'railline_miles', 397.04901686391366 kept for better accuracy.

corr_col_todrop = {'cemetery_dist_miles', 'starbucks_miles', 'dentist_dist_miles',
                  'nationalhighway_miles', 'hospital_miles', 'physician_dist_miles'
                  }

corr_Xtrain = label_Xtrain.drop(corr_col_todrop,axis=1)
corr_Xvalid = label_Xvalid.drop(corr_col_todrop,axis=1)

#print("RMSE :",rf_score_dataset(corr_Xtrain, corr_Xvalid, ytrain, yvalid))

# for min_max scaling

#num_cols = corr_Xtrain.select_dtypes(exclude='object').columns

# Standardization applies only to Numerical variables
straindata = corr_Xtrain.copy()
svaliddata = corr_Xvalid.copy()

## apply standardization on numerical features
#for i in num_cols[1:]:
#    
#    # fit on training data column
#    scaler = StandardScaler().fit(straindata[[i]])
#    
#    # transform the training data column
#    straindata[i] = scaler.fit_transform(straindata[[i]])
#    
#    
#    svaliddata[i] = scaler.transform(svaliddata[[i]])


scaler = StandardScaler()  
straindata = pd.DataFrame(scaler.fit_transform(straindata))  # fit and transform the training data column
svaliddata = pd.DataFrame(scaler.transform(svaliddata))      # transform the validation data column

# Imputation removed column names; put them back
straindata.columns = corr_Xtrain.columns
svaliddata.columns = corr_Xvalid.columns

# Imputation removed index names; put them back
straindata.set_index(corr_Xtrain.index,inplace=True)
svaliddata.set_index(corr_Xvalid.index,inplace=True)


# distplot is not working inside xpresso jupyter.
#sns.distplot(corr_Xtrain, ax=ax[0])
#ax[0].set_title("Original Data")
#sns.distplot(straindata, ax=ax[1])
#ax[1].set_title("Scaled data")

# plot the density plot of both initial and standardized data together to compare.
fig, ax = plt.subplots(1,2)
corr_Xtrain.plot(kind='kde', title="Original Data",ax=ax[0],figsize=(15,9))
straindata.plot(kind='kde', title="Scaled data",ax=ax[1],figsize=(15,9))
xpresso_save_plot("image_1", output_path=os.environ['XPRESSO_MOUNT_PATH'],output_folder="xjp_images/prepare_data")

#print("RMSE :",rf_score_dataset(straindata, svaliddata, ytrain, yvalid))
# Based on RMSE score for Random forest. Going to proceed with standardized data.
Xtrain_scaled = straindata.copy()
Xvalid_scaled = svaliddata.copy()
def zscore_test(m, X_t, X_v, y_t, y_v):
    # Get Zscore for full train dataset
    z = np.abs(stats.zscore(X_t))

    print("Number of outliers present using Z-score method:",pd.DataFrame(np.where(z > 3)).size)

    # Updating train dataset after removing outliers(having zscore above 3)
    Xt_Zs = X_t[(z < 3).all(axis=1)]
    yt_Zs = y_t[(z < 3).all(axis=1)]

    print("New Xtrain Shape:" ,Xt_Zs.shape ,"\nNew ytrain Shape:" , yt_Zs.shape)
    
    # Calculate Model Score and RMSE score for different models using dataset having outlier removed using Z-score method.
    modelScore, rmseScore = score_dataset(m, Xt_Zs, X_v, yt_Zs, y_v)
    return modelScore , rmseScore, Xt_Zs, yt_Zs

# (19248, 27) Original shape
def IQR_test(m, X_t, X_v, y_t, y_v):
    Q1 = X_t.quantile(0.25) # Get first quantile
    Q3 = X_t.quantile(0.75) # Get 3rd quantile
    IQR = Q3 - Q1           # Calculate Inter-quartile range


    print("Number of outliers present using IQR method:", 
          pd.DataFrame((X_t < (Q1 - 1.5 * IQR)) |(X_t > (Q3 + 1.5 * IQR))).sum().sum())

    # Updating train dataset after removing outliers using IQR method.
    Xt_IQR = X_t[~((X_t < (Q1 - 1.5 * IQR)) |(X_t > (Q3 + 1.5 * IQR))).any(axis=1)]
    yt_IQR = ytrain[Xt_IQR.index]

    print("New Xtrain Shape:" ,Xt_IQR.shape ,"\nNew ytrain Shape:" , yt_IQR.shape)
    
    # Calculate Model Score and RMSE score for different models using dataset having outlier removed using IQR method.
    modelScore, rmseScore = score_dataset(m, Xt_IQR, X_v, yt_IQR, y_v)

    return modelScore , rmseScore, Xt_IQR, yt_IQR
# Getting zscore for Crime_Rate column alone.
# As outlier removal for Crime_Rate is giving best result.
z = np.abs(stats.zscore(Xtrain_scaled.drop([
    'property_type',         # 372.3876
    'zipcode',               # 372.3876
    'latitude',              # 389
    'longitude',             # 392
    #'cemetery_dist_miles',   # 371.215
    #'nationalhighway_miles', # 389
    'railline_miles',        # 377
    #'starbucks_miles',       # 383
    'walmart_miles',         # 388
    #'hospital_miles',        # 381
    'opt_dist_miles',        # 369.817798
    'vet_dist_miles',        # 381
    'farmers_miles',         # 379
    'bed',                   # 394
    'bath',                  # 375
    'halfbath',              # 371.9698
    'sqft',                  # 377
    'garage',                # 372.3876
    'yearbuilt',             # 373
    'pool',                  # 368.4677
    'fireplace',             # 372.3876
    'patio',                 # 372.3876
    'lotsize',               # 383
    'Census_MedianIncome',   # 382
    'Unemployment',          # 401
    'EmploymentDiversity',   # 379
    #'Crime_Rate'            # 365.17925
],axis=1)))

# Get number of rows having zscore > 3. (Outlier)
print("Number of outliers present using Z-score method for Crime_rate column:",pd.DataFrame(np.where(z > 3)).size)

# store dataset after removing outliers.
Xtrain_Zs = Xtrain_scaled[(z < 3).all(axis=1)]
ytrain_Zs = ytrain[(z < 3).all(axis=1)]

# Display shape of updated dataset after Outlier removal
print("New Xtrain Shape:" ,Xtrain_Zs.shape ,"\nNew ytrain Shape:" , ytrain_Zs.shape)

# (19248, 27) Original shape
ytrain_log = np.log(ytrain_Zs)
yvalid_log = np.log(yvalid)

try:
    pickle.dump(train_data, open(f"{PICKLE_PATH}/train_data.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    pickle.dump(test_data, open(f"{PICKLE_PATH}/test_data.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    pickle.dump(Xtrain_Zs, open(f"{PICKLE_PATH}/Xtrain_Zs.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    pickle.dump(ytrain_log, open(f"{PICKLE_PATH}/ytrain_log.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    pickle.dump(yvalid_log, open(f"{PICKLE_PATH}/yvalid_log.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    pickle.dump(Xvalid_scaled, open(f"{PICKLE_PATH}/Xvalid_scaled.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    pickle.dump(yfull, open(f"{PICKLE_PATH}/yfull.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    pickle.dump(Xfull, open(f"{PICKLE_PATH}/Xfull.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    pickle.dump(num_columns, open(f"{PICKLE_PATH}/num_columns.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    pickle.dump(cat_columns, open(f"{PICKLE_PATH}/cat_columns.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    pickle.dump(X1, open(f"{PICKLE_PATH}/X1.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    pickle.dump(Xtrain, open(f"{PICKLE_PATH}/Xtrain.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    pickle.dump(Xvalid, open(f"{PICKLE_PATH}/Xvalid.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    pickle.dump(ytrain, open(f"{PICKLE_PATH}/ytrain.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    pickle.dump(yvalid, open(f"{PICKLE_PATH}/yvalid.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    pickle.dump(X_num, open(f"{PICKLE_PATH}/X_num.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    pickle.dump(na_num, open(f"{PICKLE_PATH}/na_num.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    pickle.dump(X_ntrain, open(f"{PICKLE_PATH}/X_ntrain.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    pickle.dump(X_nvalid, open(f"{PICKLE_PATH}/X_nvalid.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    pickle.dump(imputed_X_ntrain, open(f"{PICKLE_PATH}/imputed_X_ntrain.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    pickle.dump(imputed_X_nvalid, open(f"{PICKLE_PATH}/imputed_X_nvalid.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    pickle.dump(Xtrain_u1, open(f"{PICKLE_PATH}/Xtrain_u1.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    pickle.dump(Xvalid_u1, open(f"{PICKLE_PATH}/Xvalid_u1.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    pickle.dump(X_cat, open(f"{PICKLE_PATH}/X_cat.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    pickle.dump(na_cat, open(f"{PICKLE_PATH}/na_cat.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    pickle.dump(imputed_X_ctrain, open(f"{PICKLE_PATH}/imputed_X_ctrain.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    pickle.dump(imputed_X_cvalid, open(f"{PICKLE_PATH}/imputed_X_cvalid.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    pickle.dump(label_Xtrain, open(f"{PICKLE_PATH}/label_Xtrain.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    pickle.dump(label_Xvalid, open(f"{PICKLE_PATH}/label_Xvalid.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    pickle.dump(corr_col_todrop, open(f"{PICKLE_PATH}/corr_col_todrop.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    pickle.dump(corr_Xtrain, open(f"{PICKLE_PATH}/corr_Xtrain.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    pickle.dump(corr_Xvalid, open(f"{PICKLE_PATH}/corr_Xvalid.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    pickle.dump(straindata, open(f"{PICKLE_PATH}/straindata.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    pickle.dump(svaliddata, open(f"{PICKLE_PATH}/svaliddata.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    pickle.dump(Xtrain_scaled, open(f"{PICKLE_PATH}/Xtrain_scaled.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    pickle.dump(Xvalid_scaled, open(f"{PICKLE_PATH}/Xvalid_scaled.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    pickle.dump(modelScore, open(f"{PICKLE_PATH}/modelScore.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    pickle.dump(rmseScore, open(f"{PICKLE_PATH}/rmseScore.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    pickle.dump(ytrain_Zs, open(f"{PICKLE_PATH}/ytrain_Zs.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    pickle.dump(ytrain_log, open(f"{PICKLE_PATH}/ytrain_log.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    pickle.dump(yvalid_log, open(f"{PICKLE_PATH}/yvalid_log.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))

