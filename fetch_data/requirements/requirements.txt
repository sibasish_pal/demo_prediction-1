xgboost==1.2.1
pandas==0.25.3
scipy==1.5.3
matplotlib==3.0.2
numpy==1.19.2
seaborn==0.9.0
scikit_learn==0.23.2
