import os
import sys
import types
import pickle
import marshal
import warnings
import numpy as np
import pandas as pd
import seaborn as sns
from scipy import stats
import matplotlib.pyplot as plt
from xgboost import XGBRegressor
from sklearn.impute import SimpleImputer
from sklearn.preprocessing import LabelEncoder
from sklearn.tree import DecisionTreeRegressor
from sklearn.metrics import mean_squared_error
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import LinearRegression
from sklearn.neighbors import KNeighborsRegressor
from sklearn.ensemble import RandomForestRegressor
from sklearn.model_selection import train_test_split
from xpresso.ai.core.utils.jupyter_experiment_utils import xpresso_save_plot
from xpresso.ai.core.commons.exceptions.xpr_exceptions import MountPathNoneType


if not os.environ["XPRESSO_MOUNT_PATH"]:
    raise MountPathNoneType
PICKLE_PATH = os.path.join(os.environ["XPRESSO_MOUNT_PATH"], "xjp_store")

if not os.path.exists(PICKLE_PATH):
    os.makedirs(PICKLE_PATH, mode=0o777, exist_ok=True)

try:
    train_data = pickle.load(open(f"{PICKLE_PATH}/train_data.pkl", "rb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    test_data = pickle.load(open(f"{PICKLE_PATH}/test_data.pkl", "rb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))

## $xpr_param_component_name = fetch_data
## $xpr_param_component_type = pipeline_job 
## $xpr_param_global_variables = ["train_data","test_data"]



# to display all the columns of the dataframe in the notebook
pd.pandas.set_option('display.max_columns', None)




# Where to save the figures
PROJECT_ROOT_DIR = "."
CHAPTER_ID = "end_to_end_project"
IMAGES_PATH = os.path.join(PROJECT_ROOT_DIR, "images", CHAPTER_ID)
os.makedirs(IMAGES_PATH, exist_ok=True)

def save_fig(fig_id, tight_layout=True, fig_extension="png", resolution=300):
    path = os.path.join(IMAGES_PATH, fig_id + "." + fig_extension)
    print("Saving figure", fig_id)
    if tight_layout:
        plt.tight_layout()
    plt.savefig(path, format=fig_extension, dpi=resolution)

# Ignore useless warnings (see SciPy issue #5998)
warnings.filterwarnings(action="ignore", message="^internal gelsd")
# Reading train_data.csv file from local drive and stored inside train_data and index set as 'id'.
train_data = pd.read_csv('train_data.csv', index_col='id')
print("Train data Shape:" ,train_data.shape)

# Reading test_data.csv file from local drive and stored inside test_data and index set as 'id'.
test_data = pd.read_csv('test_data.csv',index_col='id')
print("Test data Shape :" ,test_data.shape)

try:
    pickle.dump(train_data, open(f"{PICKLE_PATH}/train_data.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))
try:
    pickle.dump(test_data, open(f"{PICKLE_PATH}/test_data.pkl", "wb"))
except (NameError, FileNotFoundError) as e:
    print(str(e))

